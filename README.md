
<!-- README.md is generated from README.Rmd. Please edit that file -->

# starsTiles

<!-- badges: start -->

<!-- badges: end -->

The goal of starsTiles is to …

## Installation

You can install the released version of starsTiles from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("starsTiles")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(starsTiles)
## basic example code
```

Some example data can be downloaded as follows

``` r
require(ecmwfr)
request <-
  list(
    "dataset"        = "reanalysis-era5-pressure-levels",
    "product_type"   = "reanalysis",
    "variable"       = c("temperature", 'geopotential', 'u_component_of_wind'),
    "pressure_level" = c("850", '875', '900', '925'),
    "year"           = "2000",
    "month"          = "04",
    "day"            = as.character(27:30),
    "time"           = sprintf('%02i:00', 0:23),
    "area"           = "64/-130/-64/144",
    "format"         = "netcdf",
    "target"         = "test.nc"
  )
wf_set_key('uid', 'key', service = 'cds')
ncfile <- wf_request(
  user = "uid",
  request = request,
  transfer = TRUE,
  path = "~",
  verbose = FALSE
)
```

``` r
require(leaflet)
require(leaflet.extras)
leaflet() %>% addTiles() %>% enableTileCaching() %>%
  addTiles(
    'http://127.0.0.1:3746/map/t/{z}/{x}/{y}?level=900',
    options = tileOptions(useCache = TRUE, crossOrigin = TRUE)
  ) %>%  setView(zoom = 2, lat = 30, lng = 5)
```
